const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken');
const db = require('./queries')
const bcrypt = require('bcrypt');

const app = express()
const port = process.env.PORT || 8080

const salt = "$2b$10$RTmcD/JAsO8Qp.IMrZv/fO";

const accessTokenSecret = 'hbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9TY3ODkwIiwibmFtZSI6Ikpvaas2ASwa';

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.listen(port, () => {
    console.log('Start server at port '+port+'.')
})

app.get('/', (req, res) => {
    // const hash = bcrypt.hashSync("aabbccddee", salt);
    res.json({
        message: 'Hello World',
        // hash
    })
})

app.get('/news', authenticateJWT, (req, res) => {
    let { Project } = req.user

    db.getNewsByProject(Project).then((resultQuery) => {
        let news = [];
        if (resultQuery && resultQuery.rows) {
            news = resultQuery.rows
        }
        res.json({
            data: news
        })
    }, (err) => {
        res.status(400).json({
            data: null,
            message: 'Error api get news.'
        })
    });
})

app.get('/news/:id', authenticateJWT, (req, res) => {
    let { Project, id } = req.user

    db.getNewsByIdAndProject(parseInt(req.params.id), Project).then((resultQuery) => {
        let news = [];
        if (resultQuery && resultQuery.rows && resultQuery.rows.length > 0) {
            news = resultQuery.rows[0]

            db.getReadNews(id, news.id).then((r) => {
                if (r && r.rows && r.rows.length === 0) {
                    // update read news
                    db.insertReadNews(id, news.id);
                }
            })

        }
        res.json({
            data: news
        })
    }, (err) => {
        res.status(400).json({
            data: null,
            message: 'Error api get news.'
        })
    });
})

app.post('/login', (req, res) => {
    // Read username and password from request body
    const { UserName, Password } = req.body;

    // Filter user from the users array by username and password
    let userModel = null;
    const hash = bcrypt.hashSync(Password, salt);
    db.getUserByUserNameAndPassword(UserName, hash).then((resultQuery) => {
        // console.log(res.rows)
        if (resultQuery && resultQuery.rows && resultQuery.rows.length > 0) {
            userModel = resultQuery.rows[0]
        }

        if (userModel) {
            // Generate an access token
            const accessToken = jwt.sign({
                id: userModel.id,
                UserName: userModel.UserName,
                Name: userModel.Name,
                LastName: userModel.LastName,
                Project: userModel.Project,
            }, accessTokenSecret);

            res.json({
                UserName: userModel.UserName,
                accessToken
            });
        } else {
            res.json({
                message: 'Username or password incorrect'
            });
        }
    }, (err) => {
        res.json({
            message: 'Username or password incorrect'
        });
    });
});
