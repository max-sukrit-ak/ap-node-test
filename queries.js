const Pool = require('pg').Pool
const pool = new Pool({
    user: 'test_node_js',
    host: 'postgres',
    database: 'test_node_js',
    password: 'aabbccddee',
    port: 5432,
})

const getUserByUserNameAndPassword = (UserName, Password) => {
    return pool.query('SELECT * FROM test_node_js.public."Users" WHERE "UserName" = $1 AND "Password" = $2', [UserName, Password])
}

const getNewsByProject = (Project) => {
    // return pool.query('SELECT * FROM test_node_js.public."UserData" WHERE "Project" = $1 ORDER BY "id"', [Project])
    return pool.query('select N."id", "TitleTH", "TitleEN", "DetailTH", "DetailEN", "Project", "StartDate", "StopDate" from test_node_js.public."UserData" N left join "ReadNews" R on N."id" = R."news_id" where N."Project" = $1 AND R."news_id" is null', [Project])
}

const getNewsByIdAndProject = (id, Project) => {
    return pool.query('SELECT * FROM test_node_js.public."UserData" WHERE "id" = $1 AND "Project" = $2', [id, Project])
}

const insertReadNews = (user_id, news_id) => {
    return pool.query('INSERT INTO test_node_js.public."ReadNews" (user_id, news_id) VALUES ($1, $2)', [user_id, news_id])
}

const getReadNews = (user_id, news_id) => {
    return pool.query('SELECT * FROM test_node_js.public."ReadNews" WHERE "user_id" = $1 AND "news_id" = $2', [user_id, news_id])
}

module.exports = {
    getUserByUserNameAndPassword,
    getNewsByProject,
    getNewsByIdAndProject,
    insertReadNews,
    getReadNews,
}